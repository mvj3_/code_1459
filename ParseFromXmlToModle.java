
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.huawei.constant.EnumLogClass;
import com.huawei.ui.maintab.bean.GridDataModle;
import com.huawei.ui.maintab.bean.GroupModle;
import com.huawei.util.AndroidUtil;
import com.huawei.util.DomUtil;
import com.huawei.util.LogUtil;

public class ParseFromXmlToModle {
	private static final String TAG="ParseFromXmlToModle";
	public static boolean showable=false;
	public static boolean IsShowMessage(Node node){
		if (node.getNodeType() == Element.ELEMENT_NODE) {
			NamedNodeMap attrs = node.getAttributes();
			String showMessage = attrs.item(0).getNodeValue();
			if(showMessage!=null && "1".equals(showMessage)){
				return true;
			}
			return false;
		}
		return false;
	}
	public static List<GroupModle> getData(String path) {
		List<GroupModle> groups = new ArrayList<GroupModle>();
		InputStream domis = DomUtil.loadStreamFromLocalFile(path);
		Element el = DomUtil.getRootElement(domis);
		showable=IsShowMessage(el);
		if (el.getNodeType() == Element.ELEMENT_NODE) {
			NodeList nodelist = el.getChildNodes();
			if (nodelist != null && nodelist.getLength() > 0) {
				for (int i = 0; i < nodelist.getLength(); i++) {
					Node node = nodelist.item(i);
					if (node.getNodeType() == Element.ELEMENT_NODE) {
						GroupModle modle = createGroupModle(node);
						groups.add(modle);
					}

				}
			}
		}
		return groups;
	}

	private static GridDataModle createGridDataModle(Node el) {
		GridDataModle modle = null;
		if (el.getNodeType() == Element.ELEMENT_NODE) {
			NamedNodeMap attrs = el.getAttributes();
			String id = attrs.item(0).getNodeValue();
			String name = attrs.item(1).getNodeValue();
			String icon = attrs.item(2).getNodeValue();
			String className = attrs.item(3).getNodeValue();
			LogUtil.log(EnumLogClass.i, TAG, "id=" + id + " name=" + name + " icon=" + icon
					+ " className=" + className);
			modle = new GridDataModle(id, name, icon, className);
			return modle;
		}
		return null;

	}

	private static GroupModle createGroupModle(Node el) {
		GroupModle groupModle = null;
		if (el.getNodeType() == Element.ELEMENT_NODE) {
			NamedNodeMap attrs = el.getAttributes();
			String id = attrs.item(0).getNodeValue();
			String icon = attrs.item(1).getNodeValue();
			groupModle = new GroupModle(id, icon);
			GridDataModle modle = null;
			NodeList nodelist = el.getChildNodes();
			if (nodelist != null && nodelist.getLength() > 0) {
				for (int i = 0; i < nodelist.getLength(); i++) {

					Node node = nodelist.item(i);
					if (node.getNodeType() == Element.ELEMENT_NODE) {
						modle = createGridDataModle(node);
						groupModle.addChild(modle);
					}

				}
			}
			return groupModle;
		}
		return null;

	}
}
