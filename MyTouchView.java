import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Region;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Scroller;

import com.huawei.R;
import com.huawei.constant.EnumLogClass;
import com.huawei.ui.maintab.bean.GroupModle;
import com.huawei.util.AndroidUtil;
import com.huawei.util.LogUtil;

public class MyTouchView extends ViewGroup {
	public static final String TAG = "MyTouchView";
	// constantooo
	private static final int INVALID_SCREEN = -1;
	private static final int SNAP_VELOCITY = 500;
	private static final int TOUCH_STATE_REST = 0;
	private static final int TOUCH_STATE_SCROLLING = 1;

	// bg
	// screen
	private int mDefaultScreen;
	// private boolean mFirstLayOut = true;
	private int mCurrentScreen;
	private int mNextScreen = INVALID_SCREEN;
	private Scroller mScroller;
	private int mScrollX;
	private VelocityTracker mVelocityTracker;
	private float mLastMotionX;
	private int mTouchState = TOUCH_STATE_REST;
	private int mTouchSlop;
	private int mMaximumVelocity;

	private OnGridItemClick itemClick;

	private Context context;
	
	
	private View  messagePanal;

	public MyTouchView(Context context, String path) {
		super(context);
		this.context = context;

		init(path);// init feature

	}

	// 初始化界面
	private void init(String path) {
		itemClick = new OnGridItemClick(context);
		mCurrentScreen = mDefaultScreen;
		mScroller = new Scroller(getContext());
		final ViewConfiguration configuration = ViewConfiguration.get(context);
		mTouchSlop = configuration.getScaledTouchSlop();
		mMaximumVelocity = configuration.getScaledMaximumFlingVelocity();
		//---------------------------------------
		LinearLayout temp = null;

		List<GroupModle> list = ParseFromXmlToModle.getData(path);
		
		createFromXmlToShowMessage(context);//showmessage

		if (list != null && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				GroupModle grouptemp = list.get(i);
				if (grouptemp != null) {
					temp = new LinearLayout(context);
					temp.setOrientation(LinearLayout.VERTICAL);
					addView(temp);
					if(messagePanal!=null){
						temp.addView(messagePanal);
					}
					
					GridView gv = new GridView(context);
					gv.setBackgroundResource(AndroidUtil.GetIDbyName(
							grouptemp.icon, "drawable", context));
					gv.setNumColumns(4);// 限定三列
//					gv
//							.setLayoutParams(new LayoutParams(
//									LayoutParams.FILL_PARENT,
//									LayoutParams.FILL_PARENT));
					gv.setVerticalSpacing(10);
					gv.setLayoutParams(new LinearLayout.LayoutParams(
	                          LinearLayout.LayoutParams.FILL_PARENT,
	                          LinearLayout.LayoutParams.FILL_PARENT,1
	                      ));
					gv.setGravity(Gravity.CENTER_HORIZONTAL);	
					gv.setAdapter(new MyAdapter(context, grouptemp.childs));
					gv.setOnItemClickListener(itemClick);
					temp.addView(gv);
				}

			}

		}
		showDefaultScreen();
	}
	
	private void createFromXmlToShowMessage(Context context){
		if(ParseFromXmlToModle.showable){
			messagePanal=inflate(context,R.layout.common_message_show, null);
		}
		
	}

	boolean isDefaultScreenShowing() {
		return mCurrentScreen == mDefaultScreen;
	}

	int getCurrentScreen() {
		return mCurrentScreen;
	}

	// set currentScrren
	void setCurrentScreen(int currentScreen) {
		if (!mScroller.isFinished()) {
			mScroller.forceFinished(true);
		}
		clearChildrenCache();
		mCurrentScreen = Math.max(0, Math.min(currentScreen,
				getChildCount() - 1));
		scrollTo(mCurrentScreen * getWidth(), 0);
		getChildAt(getCurrentScreen()).requestFocus();
		invalidate();
	}

	@Override
	public void addFocusables(ArrayList<View> views, int direction) {
		if (direction == View.FOCUS_LEFT) {
			if (mCurrentScreen > 0) {
				getChildAt(getCurrentScreen() - 1).addFocusables(views,
						direction);
			}
		} else if (direction == View.FOCUS_RIGHT) {
			if (mCurrentScreen < getChildCount() - 1) {
				getChildAt(getCurrentScreen() + 1).addFocusables(views,
						direction);
			}
		}
	}

	// clear cache;
	void clearChildrenCache() {
		final int count = getChildCount();
		for (int i = 0; i < count; i++) {
			final View child = getChildAt(i);
			child.setDrawingCacheEnabled(false);
			child.setDuplicateParentStateEnabled(false);
		}

	}

	// enable cache;
	void enableChildrenCache() {

		final int count = getChildCount();
		for (int i = 0; i < count; i++) {
			final View child = getChildAt(i);
			child.setDrawingCacheEnabled(true);
			child.setDuplicateParentStateEnabled(true);
		}
	}

	// show default screen
	void showDefaultScreen() {
		setCurrentScreen(mCurrentScreen);
	}

	// compute scroll how far
	@Override
	public void computeScroll() {
		if (mScroller != null && mScroller.computeScrollOffset()) {
			mScrollX = mScroller.getCurrX();
			Log.i(TAG, "mScroller.getCurrX()=" + mScroller.getCurrX());
			postInvalidate();
		} else if (mNextScreen != INVALID_SCREEN) {
			mCurrentScreen = Math.max(0, Math.min(mNextScreen,
					getChildCount() - 1));
			setCurrentScreen(getCurrentScreen());
			mNextScreen = INVALID_SCREEN;
			clearChildrenCache();
		}

	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);

		final int width = MeasureSpec.getSize(widthMeasureSpec);
		final int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		if (widthMode != MeasureSpec.EXACTLY) {
			LogUtil.log(EnumLogClass.e, TAG, TAG+" can only be used in  EXACTLY mode.");
		}

		final int height = MeasureSpec.getSize(widthMeasureSpec);
		final int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		if (heightMode != MeasureSpec.EXACTLY) {
			LogUtil.log(EnumLogClass.e, TAG, TAG+" can only be used in  EXACTLY mode.");
		}

		final int count = getChildCount();
		for (int i = 0; i < count; i++) {
			getChildAt(i).measure(widthMeasureSpec, heightMeasureSpec);
		}
		// Log.i(TAG, "measure width=" + width);
		// Log.i(TAG, "measure height=" + height);
		// mWidth = width;
		// mHeight = height;
		// if (mFirstLayOut) {
		// scrollTo(mCurrentScreen * width, 0);
		// mFirstLayOut = false;
		// }

	}

	@Override
	protected void dispatchDraw(Canvas canvas) {
		super.dispatchDraw(canvas);
		canvas.clipRect(mScrollX, 0, mScrollX + getWidth(), getHeight(),
				Region.Op.DIFFERENCE);
		boolean fastDraw = mTouchState != TOUCH_STATE_SCROLLING
				&& mNextScreen == INVALID_SCREEN;
		if (fastDraw) {
			drawChild(canvas, getChildAt(mCurrentScreen), getDrawingTime());
		} else {
			final long drawingTime = getDrawingTime();
			if (mNextScreen >= 0 && mNextScreen < getChildCount()
					&& Math.abs(mCurrentScreen - mNextScreen) == 1) {
				drawChild(canvas, getChildAt(mCurrentScreen), drawingTime);
				drawChild(canvas, getChildAt(mNextScreen), drawingTime);
			} else {
				final int count = getChildCount();
				for (int i = 0; i < count; i++) {
					drawChild(canvas, getChildAt(i), drawingTime);
				}
			}
		}
		canvas.restore();
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		int childLeft = 0;
		final int count = getChildCount();
		// 从左到右放置
		for (int i = 0; i < count; i++) {
			final View child = getChildAt(i);
			if (child.getVisibility() != View.GONE) {
				// Log.i(TAG, "child" + i + "-" + child.getMeasuredWidth());
				final int childWidth = child.getMeasuredWidth();
				child.layout(childLeft, 0, childLeft + childWidth, child
						.getMeasuredHeight());
				childLeft += childWidth;
			}
		}
	}

	private void snapToDestination(int currentScreen) {
		final int screenWidth = getWidth();
		final int whichScreen = (mScrollX + (screenWidth / 2)) / screenWidth;
		snapToScreen(whichScreen);
	}

	private void snapToScreen(int whichScreen) {
		if (!mScroller.isFinished()) {
			return;
		}
		clearChildrenCache();
		enableChildrenCache();
		whichScreen = Math.max(0, Math.min(whichScreen, getChildCount() - 1));
		boolean changeScreens = whichScreen != mCurrentScreen;
		mNextScreen = whichScreen;
		View focusedChild = getFocusedChild();
		if (focusedChild != null && changeScreens
				&& focusedChild == getChildAt(mCurrentScreen)) {
			focusedChild.clearFocus();
		}

		final int newX = whichScreen * getWidth();
		final int delta = newX - mScrollX;
		mScroller.startScroll(mScrollX, 0, delta, Math.abs(delta) * 2);
		// Log.i(TAG, "delta=" + delta);
		invalidate();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		final int count = getChildCount();
		for (int i = 0; i < count; i++) {
			final View child = getChildAt(i);
			drawChild(canvas, child, getDrawingTime());
		}
	}

	@Override
	public boolean dispatchUnhandledMove(View focused, int direction) {
		if (direction == View.FOCUS_LEFT) {
			if (getCurrentScreen() > 0) {
				snapToScreen(getCurrentScreen() - 1);
			}
		} else if (direction == View.FOCUS_RIGHT) {
			if (getCurrentScreen() < getChildCount() - 1) {
				snapToScreen(getCurrentScreen() + 1);
			}
		}
		return super.dispatchUnhandledMove(focused, direction);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		// if (isLocked) {
		// return true;
		// }
		final int action = ev.getAction();
		final float x = ev.getX();
		// final float y = ev.getY();
		switch (action) {
		case MotionEvent.ACTION_MOVE:
			final int xDiff = (int) Math.abs(x - mLastMotionX);
			// final int yDiff = (int) Math.abs(y - mLastMotionY);
			final int touchSlop = mTouchSlop;
			boolean xMoved = xDiff > touchSlop;
			if (xMoved) {
				if (xMoved) {
					mTouchState = TOUCH_STATE_SCROLLING;
					enableChildrenCache();
				}

			}
			break;
		case MotionEvent.ACTION_DOWN:
			mLastMotionX = x;
			mTouchState = mScroller.isFinished() ? TOUCH_STATE_REST
					: TOUCH_STATE_SCROLLING;
			break;
		case MotionEvent.ACTION_CANCEL:
		case MotionEvent.ACTION_UP:
			clearChildrenCache();
			mTouchState = TOUCH_STATE_REST;

		}
		return mTouchState != TOUCH_STATE_REST;
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		if (mVelocityTracker == null) {
			mVelocityTracker = VelocityTracker.obtain();
		}

		mVelocityTracker.addMovement(ev);
		final int action = ev.getAction();
		final float x = ev.getX();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
			if (!mScroller.isFinished()) {
				mScroller.abortAnimation();
			}

			mLastMotionX = x;

			break;
		case MotionEvent.ACTION_MOVE:
			if (mTouchState == TOUCH_STATE_SCROLLING) {
				final int delteX = (int) (mLastMotionX - x);
				mLastMotionX = x;
				if (delteX < 0) {
					if (mScrollX > 0) {
						scrollBy(Math.max(-mScrollX, delteX), 0);
					}

				} else if (delteX > 0) {
					final int availableToScroll = getChildAt(
							getChildCount() - 1).getRight()
							- mScrollX - getWidth();
					if (availableToScroll > 0) {
						scrollBy(Math.min(availableToScroll, delteX), 0);
					}
				}
			}
			break;

		case MotionEvent.ACTION_UP:
			if (mTouchState == TOUCH_STATE_SCROLLING) {
				final VelocityTracker velocityTracker = mVelocityTracker;
				velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
				int velocityX = (int) velocityTracker.getXVelocity();
				if (velocityX > SNAP_VELOCITY && mCurrentScreen > 0) {
					snapToScreen(mCurrentScreen - 1);
				} else if (velocityX < -SNAP_VELOCITY
						&& mCurrentScreen < getChildCount() - 1) {
					snapToScreen(mCurrentScreen + 1);

				} else {
					snapToDestination(mCurrentScreen);
	
				}
				if (mVelocityTracker != null) {
					mVelocityTracker.recycle();
					mVelocityTracker = null;
				}
			}
			mTouchState = TOUCH_STATE_REST;
			break;
		case MotionEvent.ACTION_CANCEL:
			mTouchState = TOUCH_STATE_REST;
			break;
		}

		System.out.println("total count=" + this.getChildCount());
		return true;
	}

}
