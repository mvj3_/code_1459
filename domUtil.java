﻿

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.huawei.constant.Constant;

public class DomUtil {

	/**
	 * @return 得到doc创建对象的工厂类
	 * @throws ParserConfigurationException
	 */
	public static DocumentBuilder getDocumentBuilder()
			throws ParserConfigurationException {
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
				.newInstance();
		return docBuilderFactory.newDocumentBuilder();
	}

	/**
	 * @param fileName
	 *            文件名
	 * @return 返回一个输入流对象
	 */
	public static InputStream loadStreamFromLocalFile(String fileName) {

		InputStream in = null;
		try {
			in = Class.forName(Constant.AndroidUitl_class_path)
					.getResourceAsStream("/assets/" + fileName);

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (in == null) {
			throw new RuntimeException("load file is err!");
		}

		return in;
	}

	/**
	 * @param parent
	 *            父元素
	 * @param attrName
	 *            　属性名称
	 * @param attrValue
	 *            属性值
	 * @return 返回对应的元素
	 */
	public static Element getElementByAttr(Element parent, String attrName,
			String attrValue) {
		NodeList childs = parent.getChildNodes();
		for (int i = 0; i < childs.getLength(); i++) {
			Node child = childs.item(i);
			if (child.getNodeType() == Element.ELEMENT_NODE) {
				NamedNodeMap attrs = child.getAttributes();
				for (int j = 0; j < attrs.getLength(); j++) {
					Node attr = attrs.item(j);
					String tempAttrName = attr.getNodeName();
					String tempAttrValue = attr.getNodeValue();
					if (tempAttrName.equalsIgnoreCase(attrName)
							&& tempAttrValue != null
							&& tempAttrValue.equalsIgnoreCase(attrValue)) {
						return (Element) child;
					}
				}
			}
		}
		return null;
	}

	/**
	 * @param inputStream
	 *            输入流对象
	 * @return 根元素
	 */
	public static Element getRootElement(InputStream inputStream) {

		// get the root element from the inputStream;
		if (inputStream == null) {
			throw new RuntimeException("getRootElement:the inputStream is null");
		}
		Element root = null;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document dom = builder.parse(inputStream);
			root = dom.getDocumentElement();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return root;
	}

	public static void transDomToString(Node e, StringBuffer sb) {
		switch (e.getNodeType()) {
		case Node.ELEMENT_NODE:
			String eNSP = e.getNamespaceURI();
			String eName = e.getNodeName();
			String NSPandName = eNSP == null ? eName : eNSP + eName;
			sb.append("<");
			sb.append(NSPandName);

			NamedNodeMap attrs = e.getAttributes();
			for (int j = 0; j < attrs.getLength(); j++) {
				Node attr = attrs.item(j);
				String attrName = attr.getNodeName();
				String attrValue = attr.getNodeValue();
				sb.append(" ");
				sb.append(attrName);
				sb.append("=");
				sb.append("\"");
				sb.append(attrValue);
				sb.append("\"");
				sb.append(" ");
			}
			sb.append('>');
			NodeList childs = e.getChildNodes();
			for (int i = 0; i < childs.getLength(); i++) {
				transDomToString(childs.item(i), sb);
			}
			sb.append("<");
			sb.append('/');
			sb.append(eName);
			sb.append('>');
			break;
		case Node.TEXT_NODE:
			sb.append(e.getNodeValue());
			break;
		}
	}

}
