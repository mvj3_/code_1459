import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.huawei.constant.Constant;
import com.huawei.constant.EnumLogClass;
import com.huawei.ui.maintab.bean.GridDataModle;
import com.huawei.util.AndroidUtil;
import com.huawei.util.LogUtil;

public class MyAdapter extends BaseAdapter {
	private static final String TAG="MyAdapter";
	private List<GridDataModle> list = null;
	private Context myContext = null;

	public MyAdapter(Context context, List<GridDataModle> arraylist) {
		super();
		list = arraylist;
		this.myContext = context;
	}

	@Override
	public int getCount() {
		if (list != null && list.size() > 0) {
			return list.size();
		} else {
			return 0;
		}

	}

	@Override
	public Object getItem(int arg0) {
		if (list != null && list.size() > 0) {
			return list.get(arg0);
		} else {
			return null;
		}
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parant) {
		GridDataModle bean = list.get(position);
		LayoutInflater mInflater = LayoutInflater.from(myContext);
		if (view == null && bean!=null) {
			view = mInflater.inflate(Constant.LAYOUT_GRID_ITEM_VIEW, null);
			ImageView iv = (ImageView) view.findViewById(Constant.ID_LIST_ITEM);
			TextView tv = (TextView) view.findViewById(Constant.ID_LIST_ITEM_TAG);
			LogUtil.log(EnumLogClass.i, TAG, bean.icon+"--item icon");
			iv.setBackgroundResource(AndroidUtil.GetIDbyName(bean.icon, "drawable", myContext));
			tv.setText(AndroidUtil.GetIDbyName(bean.name, "string", myContext));
			view.setTag(bean);

		} else {
			if( bean!=null){
				bean = (GridDataModle) view.getTag();
			}
		}
		return view;
	}

}
