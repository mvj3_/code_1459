
import java.util.ArrayList;

public class GroupModle {
	public String id;
	public String icon;
	public ArrayList<GridDataModle> childs;

	public GroupModle(String id, String icon) {
		super();
		this.id = id;
		this.icon = icon;
		childs=new ArrayList<GridDataModle>();
	}

	public void addChild(GridDataModle child) {
		childs.add(child);
	}

	public void remove(GridDataModle child) {
		if (child == null) {
			return;
		}
		if (childs.contains(child)) {
			childs.remove(child);
		}

	}
}
